const findMax = (arr) => {
  let max = arr[0];

  for (const it of arr) {
    if (it > max) {
      max = it;
    }
  }

  return max;
};


module.exports = {
  findMax
};

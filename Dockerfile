FROM node:14-alpine

WORKDIR /app/server

COPY package.json ./

ARG JWT_SECRET=$JWT_SECRET


ENV JWT_TOKEN=$JWT_SECRET
RUN echo $JWT_TOKEN

RUN echo $JWT_TOKEN > .test.env

RUN npm i

COPY . .

EXPOSE 3000

CMD ["npm", "start"]

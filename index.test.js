const { findMax } = require('./index');

describe('test function in index.js', () => {
  it('should test findMax function', () => {

    let items = [1,2,3,4,56,7,2,234,123,4,4,5,66,7];

    const res = findMax(items);

    expect(res).toEqual(Math.max(...items));
  })

})

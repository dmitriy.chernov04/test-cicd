const Koa = require('koa');

const app = new Koa();
const PORT = 3000;

app.use((ctx) => {
  console.log('Request received', ctx.request.method, ctx.request.url);
  ctx.status = 200;
  ctx.body = 'Hello world!';
});

app.listen(PORT, () => {
  console.log('app is listening on port ===> ', PORT);
});
